$('.flag').click(function() {
 $('html,body').animate({
      scrollTop: $('section').offset().top},
  'slow');
});

$('.read').click(function() {
	$('.read').hide();
	$('.less').addClass('block').removeClass('less');
	$('#last').slideDown('slow');
});

$('.less').click(function() {
	$('.block').addClass('less').removeClass('block');
	$('.read').show();
	$('#last').slideUp('slow');
});

$('.sec-read').click(function() {
	$('.sec-read').hide();
	$('.sec-less').addClass('block').removeClass('sec-less');
	$('#display').slideDown('slow');
});

$('.sec-less').click(function() {
	$('.block').addClass('sec-less').removeClass('block');
	$('.sec-read').show();
	$('#display').slideUp('slow');
});

document.getElementsByClassName('hamburger')[0].onclick = function() {
	this.classList.toggle('change');
	$('.left').toggleClass('show');
}

